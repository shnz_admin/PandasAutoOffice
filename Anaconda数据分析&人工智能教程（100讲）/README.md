<div align="center">
    <a href="https://github.com/zhaofeng092/python_auto_office"> <img src="https://badgen.net/badge/Github/%E7%A8%8B%E5%BA%8F%E5%91%98?icon=github&color=red"></a>
    <a href="https://mp.weixin.qq.com/s/xkZSp3606rTPN_JbLT3hSQ"> <img src="https://badgen.net/badge/follow/%E5%85%AC%E4%BC%97%E5%8F%B7?icon=rss&color=green"></a>
    <a href="https://space.bilibili.com/259649365"> <img src="https://badgen.net/badge/pick/B%E7%AB%99?icon=dependabot&color=blue"></a>
    <a href="https://mp.weixin.qq.com/s/wx-JkgOUoJhb-7ZESxl93w"> <img src="https://badgen.net/badge/join/%E4%BA%A4%E6%B5%81%E7%BE%A4?icon=atom&color=yellow"></a>
</div>

## Anaconda数据分析&人工智能教程（100讲）
### 代码说明

> 请大家自行完善这套课程的代码，并且通过pull request的形式，提交到这个仓库的文件下：[代码仓库](https://gitee.com/zhaofeng092/PandasAutoOffice/tree/main/Anaconda%E6%95%B0%E6%8D%AE%E5%88%86%E6%9E%90&%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E6%95%99%E7%A8%8B%EF%BC%88100%E8%AE%B2%EF%BC%89/%E4%BB%A3%E7%A0%81%E4%BB%93%E5%BA%93)

对于上面仓库的使用:
1. 请查看这个视频教程：[重要！GitHub的下载和使用教程~(附：开放我的社区全部代码仓)](https://www.bilibili.com/video/BV1Ry4y1m7Ai?spm_id_from=333.999.0.0)
2. 或者欢迎加我好友沟通,点击查看二维码👉[hdylw1024](https://gitee.com/zhaofeng092/python_auto_office/blob/master/%E8%B4%A6%E5%8F%B7%E5%85%B1%E7%94%A8%E8%B5%84%E6%BA%90/image/%E6%88%91%E7%9A%84%E5%BE%AE%E4%BF%A1.jpg)

> 推荐一套更适合小白的Python课程：[Python玩转数据分析](https://t7.lagounews.com/yR5ER9RJsD1B2)